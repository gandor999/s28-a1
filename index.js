console.log('Hello');

/*async function titles(){
	// Using the GET method to retrieve all items in the to do list place holder
	let objects = await fetch("https://jsonplaceholder.typicode.com/todos/", {method: "GET"}).then((response) => response.json());

	// Using map method to return titles of items and printing
	let titles = objects.map((object) => object.title);
	console.log(titles);
}

titles();

async function titlesAndStatus(){
	// Using the GET method to retrieve all items in the to do list place holder
	// Print status and title of items
	let objects = await fetch("https://jsonplaceholder.typicode.com/todos/", {method: "GET"}).then((response) => response.json());
	objects.forEach(function(object){
	console.log(`The item "${object.title}" on the list has a status of ${object.completed}`);
});
}

titlesAndStatus();*/


// Alternative code but hard to read. Do not recommend practicing for future code. The only difference here is that both should run in the background at the same time i think.
fetch("https://jsonplaceholder.typicode.com/todos/", {method: "GET"}).then(response => response.json()).then(data => {
	let titles = data.map(datum => datum.title);
	return titles;}).then(titles => console.log(titles));


// Better to write it like this
fetch("https://jsonplaceholder.typicode.com/todos/", {method: "GET"})
.then(response => response.json())
.then(data => {
	data.forEach(function(datum){
		console.log(`The item "${datum.title}" on the list has a status of ${datum.completed}`);
	})
});


// Creating a fetch request using POST method
fetch("https://jsonplaceholder.typicode.com/todos/", {method: 'POST', headers: {
		'Content-type': 'application/json'
	}, 
	body: JSON.stringify({
		userId: 1,
		id: 1,
		title: "Created To Do List Item",
		completed: false
	})
}).then((response) => response.json()).then((json) => console.log(json));


// Create a fetch request using PUT method
fetch("https://jsonplaceholder.typicode.com/todos/1", {method: 'PUT', headers: {
		'Content-type': 'application/json'
	}, 
	body: JSON.stringify({
		title: "Updated To Do List Item",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		dateCompleted: "Pending",
		userId: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));


// Create a fetch method using PATCH method
fetch("https://jsonplaceholder.typicode.com/todos/1", {method: 'PATCH', headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "27/10/21"
	})
}).then((response) => response.json()).then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {method: "DELETE"});


